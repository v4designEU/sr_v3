# SR_v3

Scene Recognition module of V4Design v3 for images and videos. 

**Information on the libraries used:**

The code is written in Python 3.7 and run in Anaconda3 environment. You can create an Anaconda3 environment and use commands given below to install the libraries used:

* Tensorflow-gpu == 1.14.0 (conda install -c anaconda tensorflow-gpu==1.14.0)
* OpenCV == 4.2.0 (conda install -c anaconda opencv==4.2.0)
* Keras-gpu == 2.2.4 (conda install -c anaconda keras-gpu==2.2.4)
* Pandas == 0.25.3 (conda install -c anaconda pandas==0.25.3)
* Natsort == 6.2.0 (conda install -c anaconda natsort==6.2.0)


**To install the component:**

1. download or clone this repository 
2. download model files as described in Readme file below
3. change paths in the script (change my_dir)
4. execute SR_v3_new_msg_error_mng.py script 



This repository contains an "input.json" file as an input example, in order to be able to run the code "SR_v3_new_msg_error_mng.py". 
To get the trained model for scene recognition **"my_newmodel_places_v4d.h5"** you can download it from this link https://drive.google.com/file/d/1JHVVMnKMLGqw-SpCXcroEbqBUMhs6O-Y/view?usp=sharing or contact: klearchos_stav@iti.gr and tpistola@iti.gr

**Note:** Credentials are required in order to run this service, please request a username and pass via email (klearchos_stav@iti.gr and tpistola@iti.gr)
