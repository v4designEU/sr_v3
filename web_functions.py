"""
Created on March 8, 2019
@author: Spyridon Symeonidis
"""

import requests
import json
from requests.auth import HTTPBasicAuth
import base64


class WebFunctions:
    USERNAME = "***"
    PASSWORD = "***"  #"v4d_pass" old pass

    @staticmethod
    def send_post(url, body, is_json=True):
        """
        Method to serve post requests for the V4Design project
        :param url: the url to send the request
        :param body: the request body, for json input it has to be either a dictionary or a JSON-valid string
        :param is_json: set that to false for posting plain text or to upload base-64 encoded files
        :return:
        """

        # send request and return response
        if is_json:
            # if body is string, convert it to dictionary
            if not type(body) is dict:
                try:
                    body = json.loads(body)
                except ValueError as e:
                    print("Body parameter is not in JSON format!")
                    return '{}'
            r = requests.post(url, json=body, auth=HTTPBasicAuth(WebFunctions.USERNAME, WebFunctions.PASSWORD))
        else:
            r = requests.post(url, data=body,  headers={'Content-Type': 'text/plain'}, auth=HTTPBasicAuth(WebFunctions.USERNAME, WebFunctions.PASSWORD))
        if r.status_code == 200:
            return r.text
        else:
            print("Error!")
            print("Response code:", r.status_code)
            print("Message:", r.text)
            return '{}'

    @staticmethod
    def download_from_url(url, filename):
        """
        Method to serve file downloads for the V4Design project
        :param url: the url of the file to download
        :param filename: the destination (path) of the downloaded file
        :return:
        """

        r = requests.get(url, allow_redirects=True, auth=HTTPBasicAuth(WebFunctions.USERNAME, WebFunctions.PASSWORD))
        if r.status_code == 200:
            open(filename, 'wb').write(r.content)
            return True
        else:
            print("Error downloading file!")
            print("Response code:", r.status_code)
            return False

    @staticmethod
    def encode_file(filename):
        with open(filename, "rb") as image_file:
            return base64.b64encode(image_file.read())


body = {"task": "CR", "entity": "video", "field": "id", "value": "5e186256-bb19-429c-af89-4037dac1a7e5"}
#print(WebFunctions.send_post('http://160.40.51.32:10010/retrieve', body))
# print(WebFunctions.download_from_url("https://farm6.staticflickr.com/5094/5501327375_fb5a57e97b_o.jpg","2.jpg"))

# FILE_UPLOADS
##body = WebFunctions.encode_file("1.jpg")  # encode file to String variable
####uploaded_file_url = WebFunctions.send_post('http://160.40.51.32:10010/upload/test', body, False)
###print(uploaded_file_url) # print the url where the file was uploaded

# try to download the uploaded file
###some_other_place = "upload.jpg"
###WebFunctions.download_from_url(uploaded_file_url, some_other_place)