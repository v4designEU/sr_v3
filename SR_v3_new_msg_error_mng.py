"""
Created on March 22, 2019
@author: Elissavet Batziou

Updated on October 30, 2019
@author: Theodora Pistola

Updated on January 23, 2020 (add pickle file to keep SR shots' history)
@author: Theodora Pistola

Updated on May 11, 2020 (v3 bus messages)
@author: Theodora Pistola

Updated on June 19, 2020 (v3 error management messages)
@author: Theodora Pistola

Updated on July 20, 2020 (v3 KB and bus messages)
@author: Klearchos Stavrothanasopoulos

Updated on Sept 28, 2020 (update on majority vote)
@author: Klearchos Stavrothanasopoulos
"""
# coding: utf-8

# ------------------- imports -------------------- #
import numpy as np
import pandas
import time
from keras.preprocessing import image
from keras.applications.imagenet_utils import preprocess_input
from keras.models import load_model
import json
import cv2
import shutil
from web_functions import WebFunctions  # .send_post
import os
import os.path
from os import path
import natsort
from datetime import datetime
import pickle
import pytz   # for timezones
import sys

import random
from itertools import groupby
import operator

## extra imports to set GPU options
import tensorflow as tf
from keras import backend as K

###################################
# TensorFlow wizardry
config = tf.ConfigProto()

# Don't pre-allocate memory; allocate as-needed
config.gpu_options.allow_growth = True

# Only allow a total of half the GPU memory to be allocated
config.gpu_options.per_process_gpu_memory_fraction = 0.3

# Create a session with the above options specified.
K.tensorflow_backend.set_session(tf.Session(config=config))

# load SR model trained on 152 classes of Places dataset
model1 = load_model('my_newmodel_places_v4d.h5')

# Define urls
url_retrieve = 'http://160.40.49.184:10010/retrieve'
url_upload_test = 'http://160.40.49.184:10010/upload/test'
url_save = 'http://160.40.49.184:10010/save/SR'
# pid = os.getpid()

# define flag for Error Management <------- 0 no error, 1 error found
found_error = 0

##########################################################################################################
try:
    #### define directory
    my_dir = r'C:\Users\tpistola\Desktop\sr_v3-master\sr_v3-master'  #'/Users/koafgeri/Desktop/V4D_Services/SR/'# enter the dir name    #

    #### load 152 selected categories of Places
    classes_145 = np.array(pandas.read_csv('categories_places_152.csv', header=None))

    #### load outdoor categories of Places
    outdoor_classes = np.array(pandas.read_csv('outdoor_buildings_Kle.csv', header=None))

    #### load all categories of Places
    # building_classes = np.array(pandas.read_csv(my_dir + 'categories_places.csv', header=None, sep=","))

    #### delete video files
    for fname in os.listdir(my_dir):
        # print(fname)
        if fname.startswith("videos_"):
            shutil.rmtree(os.path.join(my_dir, fname), ignore_errors=True)

    #### delete image files
    for imname in os.listdir(my_dir):
        if imname.startswith("images_"):
            shutil.rmtree(os.path.join(my_dir, imname), ignore_errors=True)

    old_body = {}
    #### import input json file
    with open('input.json') as data_file:  # test it using v3_input.json
        bus = json.loads(data_file.read())

        # read header
        header = bus["header"]

        # read body
        body = bus["body"]
        data = body["data"][0]

    ### write output json
    jsonfile = open(my_dir + 'output_SR_new.json', 'w')

    # print(type(bus))

    if type(bus) is dict:
        len_bus = 1
    else:
        len_bus = len(bus)

    for bus_id in range(0, len_bus):

        if len_bus > 1:
            bus = bus[bus_id]
        else:
            bus = bus



        # ------------------------------------------------------ #
        # -------------------- FOR VIDEOS ---------------------- #
        # ------------------------------------------------------ #

        if 'first_frame' in data:

            old_body = {"task": "CR", "field": "id", "entity": "video", "value": data[
                'simmo_id']}  # body = {"task": "CR", "entity": bus[k]['entity'], "field": "id", "value": bus[k]['simmo']}
            print("[INFO] Video")
            print("Simmo_id: {}, Shot Idx: {}".format(old_body["value"], data["shot_idx"]))

            # Write log file
            file = open("SR_log.txt", "a")
            file.write(
                "*** Input: {}   Simmo Id: {},  Shot Idx: {}".format(str(datetime.now(tz=None)), old_body["value"], data["shot_idx"]))

            input_dict = {"simmo_id": old_body["value"], "shot_idx": data["shot_idx"]}

            # write log file with simmo_id and shoIdx
            flag_process = 1
            log_history_file = "SR_history_pickle_v3"

            dict_info3 = {}
            if path.exists(log_history_file):
                ######## read pickle file ########
                # for reading also binary mode is important
                dbfile = open(log_history_file, 'rb')
                db = pickle.load(dbfile)
                print("DB: ", db)
                print("LENGTH OF DB: ", len(db))
                for idx in range(0, len(db)):
                    #print("idx = ", idx)
                    if "body" in db[idx]:
                        db_body = db[idx]["body"]
                        db_data = db_body["data"]
                        # if old_body["value"] == db[idx]["info"][0]["simmo_id"] and data["shot_idx"] == db[idx]["info"][0][
                        #     "shot_idx"] and data["user_style"] == db[idx]["info"][0]["user_style"]:
                        if old_body["value"] == db_data[0]["simmo_id"] and data["shot_idx"] == db_data[0][
                            "shot_idx"] and data["user_style"] == db_data[0]["user_style"]:
                            print("[INFO] Info for this shot is saved in KB!")
                            flag_process = 0
                            dict_info4 = db[idx]
                dbfile.close()
                ##################################
            else:
                db = []

            if flag_process == 1:
                # count the whole time SR takes
                start_SR = time.time()

                url_retrieve = 'http://160.40.49.184:10010/retrieve'

                # from WebFunctions import send_post
                # new file for new image
                os.chdir(my_dir)
                os.mkdir("videos_" + str(old_body["value"]))
                os.mkdir("videos_" + str(old_body["value"]) + '_2')
                os.mkdir("videos_" + str(old_body["value"]) + '_3')
                path = my_dir + '/videos_' + str(old_body["value"] + "/")
                path2 = my_dir + '/videos_' + str(old_body["value"] + '_2/')
                try:
                    json_metadata = WebFunctions.send_post(url_retrieve, old_body)
                    json_meta = json.loads(json_metadata)

                    if 'alternativeUrl' in json_meta:
                        url = json_meta['alternativeUrl']
                    elif 'url' in json_meta:
                        url = json_meta['url']
                    else:
                        url = json_meta['webPageUrl']
                        url = 'https:' + url
                    print(url)
                except:
                    print("-----> Error Management Message - Reason: Unable to get video url.")# Create error management json to send to bus
                    error_msg = {}
                    error_msg["header"] = {}
                    error_msg["header"]["sender"] = "SR"
                    error_msg["header"]["timestamp"] = str(datetime.now(tz=pytz.timezone('CET'))) + " CET"  # CET time
                    er_body = {}
                    er_body["reason"] = "Unable to get video url"
                    er_body["data"] = []
                    data2 = {}
                    data2["simmo_id"] = old_body["value"]
                    data2["shot_idx"] = data["shot_idx"]
                    data2["user_style"] = data["user_style"]
                    data2["URL"] = "N/A"
                    er_body["data"].append(data2)
                    er_body["message_for"] = []
                    er_body["message_for"].append("KB")
                    error_msg["body"] = er_body
                    with open("output_SR.json", "w") as json_file:
                        json.dump(error_msg, json_file)

                    found_error = 1
                    sys.exit()

                # get number of first and last frame of the group
                first_frame = data["first_frame"]
                last_frame = data["last_frame"]

                #download frames
                print("[INFO] Downloading frames...")
                try:
                    for frame in range(first_frame, last_frame + 1, 8):
                        url_frame = "http://160.40.49.184:10010/retrieve-mm?entity=frame&simmo=" + old_body["value"] + "&idx=" + str(frame)
                        frame_download = WebFunctions.download_from_url(url_frame, path + '/' + url_frame.split("=")[-1] + ".jpg")
                except:
                    print("-----> Error Management Message - Reason: Unable to download frame from KUL")# Create error management json to send to bus
                    error_msg = {}
                    error_msg["header"] = {}
                    error_msg["header"]["sender"] = "SR"
                    error_msg["header"]["timestamp"] = str(datetime.now(tz=pytz.timezone('CET'))) + " CET"  # CET time
                    er_body = {}
                    er_body["reason"] = "Reason: Unable to download frame from KUL"
                    er_body["data"] = []
                    data2 = {}
                    data2["simmo_id"] = old_body["value"]
                    data2["shot_idx"] = data["shot_idx"]
                    data2["user_style"] = data["user_style"]
                    data2["URL"] = "N/A"
                    er_body["data"].append(data2)
                    er_body["message_for"] = []
                    er_body["message_for"].append("KB")
                    error_msg["body"] = er_body
                    with open("output_SR.json", "w") as json_file:
                        json.dump(error_msg, json_file)

                    found_error = 1
                    sys.exit()


                # to save info for full info json
                out_info = []

                # to save info for bus json
                out_info2 = []

                # count SR time without video download and frames' extraction
                start_SR_raw = time.time()

                print("[INFO] SR in progress...")
                print("Number of total frames are:", int(last_frame) - int(first_frame))

                # get number of representative frame
                rep_frame = data["rep_frame"]



                #preparing a random list of frames for majority vote
                #randomlist = random.sample(range(first_frame, last_frame), 20)

                # sort images by numerical order
                images = natsort.natsorted(os.listdir(path))

                scene_list = []
                scene_list_dict = []
                step = int(((int(last_frame) - int(first_frame))/8) / 20)
                if step == 0:
                    step = 1
                print("Step for majority vote is:", step)


                #predicting scene for range of frames with a step
                for rand_img in range(0, len(images), step):

                    # load representative frame and predict the depicted scene
                    img_path = path + '/' + (images[rand_img])

                    img = image.load_img(img_path, target_size=(224, 224))
                    x = image.img_to_array(img)
                    x = np.expand_dims(x, axis=0)
                    x = preprocess_input(x, mode='caffe')

                    preds1 = model1.predict(x)
                    a = str(images[rand_img])  #### image name
                    b = old_body["value"]  ####simmo id
                    c = np.amax(preds1)  #### maximum value - max probability
                    d = np.argmax(preds1)
                    print("Max val:", c, " Scene:", classes_145[d, 1])

                    dict_scene = {}
                    dict_scene["scene"] = classes_145[d, 1]
                    dict_scene["prob"] = c

                    scene_list.append(classes_145[d, 1])
                    scene_list_dict.append(dict_scene)

                #counting duplicates
                my_dict = {i: scene_list.count(i) for i in scene_list}
                print(my_dict)

                max_scene = max(my_dict.items(), key=operator.itemgetter(1))[0]
                sc_list = []

                for sc in scene_list_dict:
                    if sc["scene"] == max_scene:
                        sc_list.append(sc["prob"])

                c = max(sc_list)
                scene_recognized = max_scene
                print("Scene:", scene_recognized, " Prob:", c)

                if not scene_recognized in outdoor_classes[:, 1]:
                    outd = False
                    ind = True
                else:
                    outd = True
                    ind = False
                print("Outdoor: ", outd)

                # FOR KB

                dict_info = {}
                dict_info["simmo"] = []
                dict_info["shotIdx"] = []
                dict_info["first_frame"] = []
                dict_info["last_frame"] = []
                dict_info["building"] = []
                dict_info["probability_building"] = []
                dict_info["outdoor"] = []

                dict_info["simmo"] = b
                dict_info["shotIdx"] = data["shot_idx"]
                dict_info["first_frame"] = first_frame
                dict_info["last_frame"] = last_frame
                dict_info["building"] = scene_recognized
                dict_info["probability_building"] = str(c)
                dict_info["outdoor"] = outd

                print(dict_info)
                out_info = dict_info

                try:
                    json_to_datastorage = WebFunctions.send_post(url_save, dict_info)
                except:
                    print(
                        "-----> Error Management Message - Reason: DSRS unreachable.")  # Create error management json to send to bus
                    error_msg = {}
                    error_msg["header"] = {}
                    error_msg["header"]["sender"] = "SR"
                    error_msg["header"]["timestamp"] = str(
                        datetime.now(tz=pytz.timezone('CET'))) + " CET"  # CET time
                    er_body = {}
                    er_body["reason"] = "Unable to save info in KB"
                    er_body["data"] = []
                    data2 = {}
                    data2["simmo"] = old_body["value"]
                    data2["shot_idx"] = data["shot_idx"]
                    data2["user_style"] = data["user_style"]
                    data2["URL"] = url
                    er_body["data"].append(data2)
                    er_body["message_for"] = []
                    er_body["message_for"].append("KB")
                    error_msg["body"] = er_body
                    with open("output_SR.json", "w") as json_file:
                        json.dump(error_msg, json_file)

                    found_error = 1
                    sys.exit()

                # For BUS
                if step > 1:
                    step = 8  # sent to bus info every 8 frames   <---------------------- STEP
                for k in range(0, len(images)):
                    dict_info2 = {}
                    dict_info2["simmo_id"] = []
                    dict_info2["media_id"] = []
                    dict_info2["entity"] = []
                    dict_info2["frame"] = []
                    dict_info2["outdoor"] = []
                    dict_info2["shot_idx"] = []
                    dict_info2["user_style"] = []

                    f = str(images[k].split("_")[-1].split(".")[0])

                    dict_info2["simmo_id"] = b
                    dict_info2["media_id"] = json_to_datastorage
                    dict_info2["entity"] = "video"
                    dict_info2["frame"] = f
                    dict_info2["outdoor"] = outd
                    dict_info2["shot_idx"] = data["shot_idx"]
                    dict_info2["user_style"] = data["user_style"]

                    out_info2.append(dict_info2)



                print("[INFO] Writing json files....")
                # write full json file
                with open(path2 + 'my_output_SR.json', 'w') as outfile:
                    json.dump(out_info, outfile)

                # dict_info3 = {}    # ??
                dict_info4 = {}
                dict_info3["timestamp"] = str(datetime.now(tz=pytz.timezone('CET'))) + " CET"  # CET time      #str(datetime.now(tz=None))
                dict_info3["sender"] = "SR"
                # dict_info3["info"] = out_info2
                dict_info4["header"] = dict_info3
                dict_info4["body"] = {}
                dict1 = {}
                dict1["data"] = out_info2
                dict_info4["body"] = dict1

                # write json file for bus
                with open('output_SR_old.json', 'w') as outfile:
                    json.dump(out_info2, outfile)

                    # input_dict["output"] = dict_info3
                db.append(dict_info4)

                print("[INFO] Appended DB: ", db)

                ########## write info in pickle file ##########
                dbfile = open(log_history_file, 'wb')

                # source, destination
                pickle.dump(db, dbfile)
                dbfile.close()
                ################################################

                end_SR_raw = time.time() - start_SR_raw
                end_SR = time.time() - start_SR
                print("SR for video took {} sec with video downloading time.".format(end_SR))
                print("SR for video took {} sec without video downloading time.".format(end_SR_raw))

            # write json file for bus
            with open('output_SR.json', 'w') as outfile:
                json.dump(dict_info4, outfile)

            file.write("\n*** Output: {}\n\n".format(str(datetime.now(tz=None))))
            file.close()
except:
    if found_error == 0:
        print("-----> error management message - reason: n/a")

        # create error management json to send to bus
        error_msg = {}
        error_msg["header"] = {}
        error_msg["header"]["sender"] = "sr"
        error_msg["header"]["timestamp"] = str(datetime.now(tz=pytz.timezone('cet'))) + " cet"  # cet time
        er_body = {}
        er_body["reason"] = "n/a"
        er_body["data"] = []
        data2 = {}
        if "value" in old_body.keys():
            data2["simmo_id"] = old_body["value"]
            data2["shot_idx"] = data["shot_idx"]
            data2["user_style"] = data["user_style"]
            data2["url"] = url
        else:

            er_body["reason"] = "wrong input json file"

        er_body["data"].append(data2)
        er_body["message_for"] = []
        er_body["message_for"].append("kb")
        error_msg["body"] = er_body
        with open("output_sr.json", "w") as json_file:
            json.dump(error_msg, json_file)








